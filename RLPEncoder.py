import os
import rlp
from web3 import Web3
from eth_account import Account, messages
from dotenv import load_dotenv

load_dotenv()


def encoder_and_sign(inp, private_key):
    encoded_data = rlp.encode(inp).hex()
    sha3 = Web3.keccak(hexstr=encoded_data).hex()
    message = messages.encode_defunct(hexstr=sha3)
    signed_message = Account.sign_message(message, private_key=private_key)
    return '0x' + encoded_data, signed_message.signature.hex()


def pack_and_sign(types, values, private_key):
    encoded_data = Web3.solidityKeccak(types, values).hex()
    encoded_data = Web3.keccak(hexstr=encoded_data).hex()
    message = messages.encode_defunct(hexstr=encoded_data)
    signed_message = Account.sign_message(message, private_key=private_key)
    return signed_message


if __name__ == '__main__':
    private_key_hex = os.environ.get("PRIVATE_KEY")
    print(private_key_hex)
    # print(type(0x6DeFAd2d9841203B9C7062c143FEd7295d065aaE))
    #
    # print(encoder_and_sign([0x6DeFAd2d9841203B9C7062c143FEd7295d065aaE, 12, 1], private_key_hex))

    # tokenAddress, camp_id, user_address, amount, deadline, claim_Id
    types = ['address', 'string', 'address', 'uint256', 'uint256', 'string']
    values = ["0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56", "32067858-0700-446d-afd1-a73d05629571", '0x4dB37221F245D3aA8E4774FeA7fB7485F868bA4C', 50000000000000000000, 10000000000, '123']
    res = pack_and_sign(types, values, private_key_hex)

    print(res["signature"].hex())


