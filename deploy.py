# import the following dependencies
import json
from web3 import Web3
import asyncio
from config import BlockChain
import threading
import json
from web3.gas_strategies.rpc import rpc_gas_price_strategy
from web3.middleware import geth_poa_middleware
# importing module
import logging
import time

# Create and configure logger
logging.basicConfig(filename="loggingFile.log",
                    format='%(asctime)s %(message)s',
                    filemode='a')

# Creating an object
logger = logging.getLogger()

# Setting the threshold of logger to DEBUG
logger.setLevel(logging.INFO)


# add your blockchain connection information
web3 = Web3(Web3.HTTPProvider(BlockChain.PROVIDER))
web3.middleware_onion.inject(geth_poa_middleware, layer=0)
web3.eth.set_gas_price_strategy(rpc_gas_price_strategy)

# 2. Create address variables
address_from = "0x6DeFAd2d9841203B9C7062c143FEd7295d065aaE"
address_to = "0x4dB37221F245D3aA8E4774FeA7fB7485F868bA4C"

# 3. Fetch balance data
balance_from = web3.fromWei(web3.eth.get_balance(address_from), "ether")
balance_to = web3.fromWei(web3.eth.get_balance(address_to), "ether")

print(f"The balance of { address_from } is: { balance_from } BNB")
print(f"The balance of { address_to } is: { balance_to } BNB")


# 3. Create address variables
account_from = {
    "private_key": "0x2c950eba9f6ddbc30db5d607f8db31b4aed7fa12fa9f74b8997cb09fb9af609e",
    "address": "0x6DeFAd2d9841203B9C7062c143FEd7295d065aaE",
}


def create_contract_instance(address, abi_path):
    with open(abi_path, 'r', encoding='utf-8') as f:
        abi = f.read()
    abi = json.loads(abi)

    contract = web3.eth.contract(address=address, abi=abi)
    return contract


def create_airdrop_contract(router_instance, camp_id):
    time.sleep(1)
    tx = router_instance.functions.createPool(camp_id).buildTransaction({
        'from': account_from['address'],
        'nonce': web3.eth.get_transaction_count(account_from['address']),
    })

    # Sign tx with PK
    tx_create = web3.eth.account.sign_transaction(tx, account_from['private_key'])

    # Send tx and wait for receipt
    tx_hash = web3.eth.send_raw_transaction(tx_create.rawTransaction)
    tx_receipt = web3.eth.wait_for_transaction_receipt(tx_hash)
    print(f'Create airdrop successful with hash: {tx_receipt.transactionHash.hex()}')


def config_pool(router_instance, token_address, camp_id):
    time.sleep(1)
    start_date = int(time.time()) + 100
    claim_date = start_date + 100
    end_date = start_date + 30*24*3600
    print(start_date, claim_date, end_date)
    liquid = BlockChain.LIQUID
    max_claim = BlockChain.MAX_CLAIM
    tx = router_instance.functions.configPool(token_address, camp_id, start_date, claim_date, end_date, liquid, max_claim).buildTransaction({
        'from': account_from['address'],
        'nonce': web3.eth.get_transaction_count(account_from['address']),
    })

    # Sign tx with PK
    tx_create = web3.eth.account.sign_transaction(tx, account_from['private_key'])

    # Send tx and wait for receipt
    tx_hash = web3.eth.send_raw_transaction(tx_create.rawTransaction)
    tx_receipt = web3.eth.wait_for_transaction_receipt(tx_hash)
    print(f'Config pool successful with hash: {tx_receipt.transactionHash.hex()}')


def add_liquid(router_instance, camp_id):
    time.sleep(1)

    tx = router_instance.functions.addLiquidity(camp_id).buildTransaction({
        'from': account_from['address'],
        'nonce': web3.eth.get_transaction_count(account_from['address']),
    })

    # Sign tx with PK
    tx_create = web3.eth.account.sign_transaction(tx, account_from['private_key'])

    # Send tx and wait for receipt
    tx_hash = web3.eth.send_raw_transaction(tx_create.rawTransaction)
    tx_receipt = web3.eth.wait_for_transaction_receipt(tx_hash)
    print(f'Add liquid successful with hash: {tx_receipt.transactionHash.hex()}')


def get_airdrop_contract(router_instance, camp_id):
    time.sleep(1)
    contract = router_instance.functions.getPool(camp_id).call()
    return contract


def approve_token(erc20_instance, to, amount):
    time.sleep(1)
    tx = erc20_instance.functions.approve(to, amount).buildTransaction({
        'from': account_from['address'],
        'nonce': web3.eth.get_transaction_count(account_from['address']),
    })

    # Sign tx with PK
    tx_create = web3.eth.account.sign_transaction(tx, account_from['private_key'])

    # Send tx and wait for receipt
    tx_hash = web3.eth.send_raw_transaction(tx_create.rawTransaction)
    tx_receipt = web3.eth.wait_for_transaction_receipt(tx_hash)
    print(f'Tx successful with hash: {tx_receipt.transactionHash.hex()}')


def full_flow(token_address, camp_id):
    # 1. Init instances
    airdrop_router = create_contract_instance(BlockChain.AIRDROP_ROUTER, BlockChain.AIRDROP_ROUTER_ABI)
    erc20 = create_contract_instance(token_address, BlockChain.ERC20_ABI)

    # 2. Create Airdrop contract
    create_airdrop_contract(airdrop_router, camp_id)

    # 3. Create and config pool
    config_pool(airdrop_router, erc20.address, camp_id)

    # 4. Get airdrop contract address
    airdrop_contract = get_airdrop_contract(airdrop_router, camp_id)
    print(f'campId: {camp_id} -> Contract: {airdrop_contract} ')
    logger.info(f'campId: {camp_id} -> Contract: {airdrop_contract}')
    # 5. Approve token
    approve_token(erc20, airdrop_contract, amount=int(BlockChain.LIQUID*105//100))

    # 6. Add liquid
    add_liquid(airdrop_router, camp_id)

    print(erc20.functions.balanceOf(airdrop_contract).call())


def get_contract(token_address, camp_id):
    # 1. Init instances
    airdrop_router = create_contract_instance(BlockChain.AIRDROP_ROUTER, BlockChain.AIRDROP_ROUTER_ABI)
    erc20 = create_contract_instance(token_address, BlockChain.ERC20_ABI)

    # # 2. Create Airdrop contract
    # create_airdrop_contract(airdrop_router, erc20.address, camp_id)
    #
    # # 3. Create and config pool
    # create_and_config_pool(airdrop_router, erc20.address, camp_id)

    # 4. Get airdrop contract address
    airdrop_contract = get_airdrop_contract(airdrop_router, camp_id)
    print(f'campId: {camp_id} -> Contract: {airdrop_contract} ')
    logger.info(f'campId: {camp_id} -> Contract: {airdrop_contract}')


def get_claim_id(camp_id, claim_id) -> bool:
    # init instance
    airdrop_router = create_contract_instance(BlockChain.AIRDROP_ROUTER, BlockChain.AIRDROP_ROUTER_ABI)
    claimed = airdrop_router.functions.getClaimIdUsed(camp_id, claim_id).call()
    return claimed


if __name__ == '__main__':

    logger.info("Start")

    Zon_camp_id = '0ac6b98c-9e61-406f-8b64-3e20888a4086'
    C98_camp_id = '9a3b41ae-4082-4613-a8a3-89df349885ba'
    Kyber_camp_id = '8b200b52-b5b2-43c1-ad37-3e96639889e1'
    Houbi_camp_id = 'b3f64239-d675-4bb0-ad9d-eaef89a78707'
    Ancient8_camp_id = 'ef815d34-404e-427e-80ef-7401b2c37443'  # -> A8
    Huuk = '5cdb9001-bc24-44fa-9525-766ff78dd46d'  # -> Jade
    YGG_SEA = '23f7fe19-9ade-43fd-ae72-13b705d30756'  # -> Jade
    Baryon_network = '691ccbd6-b063-44e6-acaf-49cf31420d90'  # -> Jade
    Binance = 'c43efd85-ddf1-4297-8757-94dec0129565'  # -> Jade
    a16z = '647660d3-d81a-471d-9703-040500c041fd'  # -> Jade
    Shima = '2ae36d3c-bc9a-4df2-8a54-483c92f3c299'  # -> Jade
    LD_Capital = 'd2f9552e-c390-4a22-b0cf-bde661d0fd26'  # -> Jade
    UniSwap = '1251f43c-1ae1-4d5b-8f3e-b50c3c180c2a'
    OldFashion = '7350e174-82ea-44f6-857b-c53fbc96718c'
    Wixdom = '3edfe6ae-6ec7-4deb-9247-380d61a1c474'
    Wixdom_2 = 'a7953016-7e81-4966-b210-b6de04df9fae'
    Wix_prod = '32067858-0700-446d-afd1-a73d05629571'

    # full_flow(BlockChain.ZON, Zon_camp_id)
    # full_flow(BlockChain.C98, C98_camp_id)
    # full_flow(BlockChain.KNC, Kyber_camp_id)
    # full_flow(BlockChain.HT, Houbi_camp_id)
    # full_flow(BlockChain.A8, Ancient8_camp_id)
    # full_flow(BlockChain.JADE, Huuk)
    # full_flow(BlockChain.JADE, YGG_SEA)
    # full_flow(BlockChain.JADE, Baryon_network)
    # full_flow(BlockChain.JADE, Binance)
    # full_flow(BlockChain.JADE, a16z)
    # full_flow(BlockChain.JADE, Shima)
    # full_flow(BlockChain.JADE, LD_Capital)
    # full_flow(BlockChain.JADE, UniSwap)
    # full_flow(BlockChain.JADE, OldFashion)
    # full_flow(BlockChain.USDC, Wixdom_2)
    full_flow(BlockChain.BUSD, Wix_prod)

    # full_flow(BlockChain.JADE, OldFashion)

